# Automated Robotic Manipulator Construction
![](https://i.imgur.com/Tn4UyAU.png "Full Setup")

This program was developed during my semester thesis. It serves to construct a manipulator and its control in an automated way. The program is written using the **SG-library** (https://sg-lib.org/) developed by Prof. Lüth at MiMed TUM.

## Manipulator Examples

A standard manipulator with two arms
![](https://i.imgur.com/pl9fNwf.png "Dual Manipulator")

A small manipulator used as the tip of a longer tool
![](https://i.imgur.com/ialdulC.png "Tip Manipulator")

Example of a single element of a manipulator
![](https://i.imgur.com/MrTJvX6.png "Element")

Example of a control unit
![](https://i.imgur.com/YalDv0l.png "Control UNit")

Example of a single control unit
![](https://i.imgur.com/bTpIBAx.png "Single Control Unit")

